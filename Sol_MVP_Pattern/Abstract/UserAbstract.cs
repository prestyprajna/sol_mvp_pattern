﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract
{
    public abstract class UserAbstract
    {
        public abstract  Task<List<CityEntity>> GetCityData();

        protected abstract  Task<string> InsertUserData(UserEntity userEntityObj);
    }
}
