﻿CREATE PROCEDURE [dbo].[uspGetUser]
	@Command VARCHAR(MAX),
		
	@UserId NUMERIC(18,0),	
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	--@DateOfBirth DATETIME,
	--@Age INT,
	--@Gender BIT,
	--@CityId INT,
	--@ImagePath NVARCHAR(MAX),
	@MobileNo VARCHAR(10),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	
		IF @Command='SELECT'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserID,
				U.FirstName,
				U.LastName,
				u.MobileNo			
					FROM tblUser AS U
						

				SET @Status=1
				SET @Message='SELECT SUCCESSFULL'

					COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

		ELSE IF @Command='SELECT_UserData_By_ID'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserID,
				U.FirstName,
				U.LastName,
				U.DateOfBirth,
				U.Age,
				U.Gender,
				U.CityId,
				U.ImagePath		,
				U.MobileNo				
					FROM tblUser AS U
						WHERE U.UserID=@UserId

				SET @Status=1
				SET @Message='SELECT BY ID SUCCESSFULL'

					COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SELECT BY ID EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

		ELSE IF @Command='SEARCH'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				
				SELECT U.UserID,
				U.FirstName,
				U.LastName,
				U.DateOfBirth,
				U.Age,
				U.Gender,
				U.CityId,
				U.ImagePath,
				U.MobileNo		
					FROM tblUser AS U
						WHERE U.FirstName=@FirstName
						OR
						U.LastName=@LastName
						--OR
						--U.DateOfBirth=@DateOfBirth
						OR
						U.MobileNo=@MobileNo
						

				SET @Status=1
				SET @Message='SEARCH SUCCESSFULL'

					COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='SEARCH EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END


		

	END