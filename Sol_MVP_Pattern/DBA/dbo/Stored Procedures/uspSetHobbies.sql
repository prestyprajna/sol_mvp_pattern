﻿CREATE PROCEDURE uspSetHobbies
(
	@Command VARCHAR(MAX),

	@HobbyId NUMERIC(18,0),
	@HobbyName VARCHAR(50),
	@Interested BIT,
	@UserId NUMERIC(18,0),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

	--declaration
	DECLARE @ErrorMessage VARCHAR(MAX)

	
		IF @Command='INSERT'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY
				INSERT INTO tblHobbies
				(
					HobbyName,
					Interested,
					UserId
				)
				VALUES
				(
					@HobbyName,
					@Interested,
					@UserId
				)

				SET @Status=1
				SET @Message='INSERT SUCCESSFULL'

					COMMIT TRANSACTION


			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='INSERT EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

		ELSE IF @Command='UPDATE'
		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY			
			
				SELECT 
				--@HobbyId=CASE WHEN @HobbyId IS NULL THEN H.HobbyId  ELSE @HobbyId END,
				@HobbyName=CASE WHEN @HobbyName IS NULL THEN H.HobbyName  ELSE @HobbyName END,
				@Interested=CASE WHEN @Interested IS NULL THEN H.Interested  ELSE @Interested END,
				@UserId=CASE WHEN @UserId IS NULL THEN H.UserId  ELSE @UserId END
					FROM tblHobbies AS H
						WHERE H.UserId=@UserId
						AND
						H.HobbyId=@HobbyId

				UPDATE tblHobbies
					SET-- HobbyId=@HobbyId,
					HobbyName=@HobbyName,
					Interested=@Interested,
					UserId=@UserId
						WHERE UserId=@UserId
						AND
						HobbyId=@HobbyId




				SET @Status=1
				SET @Message='UPDATE SUCCESSFULL'

					COMMIT TRANSACTION


			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='UPDATE EXCEPTION'

				ROLLBACK TRANSACTION
				RAISERROR(@ErrorMessage,16,1)
				
			END CATCH

		END

	END