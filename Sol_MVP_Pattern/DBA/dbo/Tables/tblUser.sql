﻿CREATE TABLE [dbo].[tblUser] (
    [UserID]      NUMERIC (18)   IDENTITY (1, 1) NOT NULL,
    [FirstName]   VARCHAR (50)   NULL,
    [LastName]    VARCHAR (50)   NULL,
    [Gender]      BIT            NULL,
    [DateOfBirth] DATETIME       NULL,
    [Age]         INT            NULL,
    [CityId]      NUMERIC (18)   NULL,
    [ImagePath]   NVARCHAR (MAX) NULL,
    [MobileNo]    VARCHAR (10)   NULL,
    PRIMARY KEY CLUSTERED ([UserID] ASC)
);

