﻿using BLL;
using Entity;
using Sol_MVP.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_MVP.Presenter
{
    public class UserPresenter: UserContext
    {
        #region Declaration
        private IUserView _userView = null;
        #endregion

        #region Constructor
        public UserPresenter()
        {

        }

        public UserPresenter(IUserView userView)
        {
            this._userView = userView;
        }
        #endregion

        #region  public methods

        private async Task<UserEntity> DataMapping()
        {
            return await Task.Run(() => {

                return new UserEntity()
                {
                    FirstName = _userView.FirstNameBinding,
                    LastName = _userView.LastNameBinding,
                    DateOfBirth = _userView.DateOfBirthBinding,
                    Age=_userView.AgeBinding,
                    Gender=_userView.GenderBinding,
                    MobileNo=_userView.MobileNoBinding,
                    hobbyEntityObj=_userView.HobbyBinding,
                    CityId=_userView.CityBinding
                };

            });
        }

        public async Task<string> AddData()
        {
            return await Task.Run(async() =>
            {
                string result = await base.InsertUserData(this.DataMapping().Result);

                return result;
            });
        }

        #endregion
    }
}