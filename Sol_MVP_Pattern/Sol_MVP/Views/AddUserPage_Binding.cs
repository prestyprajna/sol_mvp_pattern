﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Sol_MVP.Views
{
    public partial class AddUserPage: IUserView
    {
        public string FirstNameBinding
        {
            get  //ui to entity   
            {
                return txtFirstName.Text;
            }
            set  //entity to ui
            {
                txtFirstName.Text = value;
            }
        }

        public string LastNameBinding
        {
            get
            {
                return txtLastName.Text;
            }
            set
            {
                txtLastName.Text = value;
            }
        }

        public dynamic GenderBinding
        {
            get
            {
                return (rblGender
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Where((leListObj) => leListObj.Selected == true)
                    ?.FirstOrDefault()
                    ?.Text) == "Male" ? true : false;               
                
            }

         
            set
            {
                bool flag = value;

                rblGender?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.ForEach((leListObj) =>
                    {
                        if(flag==true)
                        {
                            if(leListObj.Text=="Male")
                            {
                                leListObj.Selected = true;
                            }
                        }
                        else
                        {
                            if(leListObj.Text=="Female")
                            {
                                leListObj.Selected = true;
                            }
                        }
                    });


                //bool flag = Convert.ToBoolean(value); 

                //if(flag==true)
                //{
                //    if(rblGender.Text=="Male")
                //    {
                //        rblGender.Text = Convert.ToString(flag);
                //    }
                //    else if(rblGender.Text=="Female")
                //    {
                //        flag = Convert.ToBoolean(0);
                //        rblGender.Text = Convert.ToString(flag);
                //    }
                //}
            }
        }

        public string DateOfBirthBinding
        {
            get
            {
                return
                    txtDateBirth.Text;
            }
            set
            {
                txtDateBirth.Text = value;
            }
        }

        public int? AgeBinding
        {
            get
            {
                return
                    Convert.ToInt32(txtAge.Text);
            }
            set
            {
                txtAge.Text = value.ToString();
            }
        }

        public dynamic CityBinding
        {
            get
            {
                return Convert.ToInt32(ddlCity.SelectedValue);
            }
            set
            {
                if(value is List<CityEntity>)
                {
                    ddlCity.DataSource = value;
                    ddlCity.DataBind();

                    ddlCity.AppendDataBoundItems = true;
                    ddlCity.Items.Insert(0, new ListItem("--select city--", "0"));
                    ddlCity.SelectedIndex = 0;
                }
                else if(value is decimal)
                {
                    ddlCity.SelectedValue = value;
                }
               

            }
        }

        //public string ImagePathBinding { get; set; }

        public string MobileNoBinding
        {
            get
            {
                return
                    txtMobileNo.Text;
            }
            set
            {
                txtMobileNo.Text = value;
            }
        }

        public List<HobbyEntity> HobbyBinding
        {
            get
            {
                return
                chkHobbies?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.Select((leListObj) => new HobbyEntity()
                    {
                        HobbyName = leListObj?.Text,
                        Interested = leListObj?.Selected
                    })
                    ?.ToList();
            }
            set
            {
                var getUserHobbiesObj = value;

                chkHobbies.DataSource = getUserHobbiesObj;
                chkHobbies.DataBind();

                chkHobbies
                    ?.Items
                    ?.Cast<ListItem>()
                    ?.AsEnumerable()
                    ?.ToList()
                    ?.ForEach((leListObj) =>
                    {
                        leListObj.Selected = Convert.ToBoolean(getUserHobbiesObj
                                            ?.AsEnumerable()
                                            ?.FirstOrDefault((leHobbyEntityObj) => leHobbyEntityObj.HobbyName == leListObj.Text)
                                            ?.Interested);
                    });
            }
        }
    }
}