﻿using DAL;
using Entity;
using Sol_MVP.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_MVP.Views
{
    public partial class AddUserPage : System.Web.UI.Page
    {
        protected async void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack==false)
            {
                List<CityEntity> cityEntityObj= await new UserDal().GetCityData();

                this.CityBinding = cityEntityObj;                               
            }
        }

        protected async void btnSubmit_Click(object sender, EventArgs e)
        {
            // Create an Instance of Presenter
            UserPresenter userPresnterObj = new UserPresenter(this);
            await userPresnterObj.AddData();
        }


    }
}