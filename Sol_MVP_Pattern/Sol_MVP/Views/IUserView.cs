﻿using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_MVP.Views
{
    public interface IUserView
    {
        string FirstNameBinding { get; set; }

        string LastNameBinding { get; set; }

        dynamic GenderBinding { get; set; }

        string DateOfBirthBinding { get; set; }

        int? AgeBinding { get; set; }

        dynamic CityBinding { get; set; }

        List<HobbyEntity> HobbyBinding { get; set; }

        //string ImagePathBinding { get; set; }

        string MobileNoBinding { get; set; }
    }
}
