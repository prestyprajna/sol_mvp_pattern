﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="AddUserPage.aspx.cs" Inherits="Sol_MVP.Views.AddUserPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
       
         .imageClass{
             width:100px;
             height:100px;
             border-radius:150px;
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel id="updatePanel" runat="server">
            <ContentTemplate>

                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblFirstName" runat="server" Text="FirstName : "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblLastName" runat="server" Text="LastName : "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblGender" runat="server" Text="Gender : "></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rblGender" runat="server" >
                                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblDateOfBirth" runat="server" Text="Date Of Birth : "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtDateBirth" runat="server" TextMode="Date"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="lblAge" runat="server" Text="Age : "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAge" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                     <tr>
                        <td>
                            <asp:Label ID="tlbCity" runat="server" Text="City : "></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCity" runat="server" DataTextField="CityName" DataValueField="CityId"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                         <td>
                            <asp:Label ID="lbHobbies" runat="server" Text="Hobbies : "></asp:Label>
                        </td>
                        <td>
                            <asp:CheckBoxList ID="chkHobbies" runat="server">
                                <asp:ListItem Text="Reading" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Drawing" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Painting" Value="3"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>

                   <%--  <tr>
                        <td>
                            <asp:Label ID="lbImagePath" runat="server" Text="Image : " CssClass="imageClass"></asp:Label>
                        </td>
                        <td>
                            <asp:Image ID="imgPath" runat="server" />
                        </td>
                    </tr>--%>

                     <tr>
                        <td>
                            <asp:Label ID="lblMobileNo" runat="server" Text="Mobile No : "></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtMobileNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>
                </table>


            </ContentTemplate>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
