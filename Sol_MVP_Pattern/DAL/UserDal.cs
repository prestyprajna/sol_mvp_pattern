﻿using Abstract;
using DAL.ORD;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class UserDal: UserAbstract
    {
        #region  declaration
        private UserDCDataContext dc = null;

        #endregion

        #region  constructor
        public UserDal()
        {
            dc = new UserDCDataContext();
        }

        #endregion

        #region  public methods

        public async override Task<List<CityEntity>> GetCityData()
        {
            return await Task.Run(() =>
            {
                int? status = null;
                string message = null;

                var getCityData = dc?.uspGetCities(
                    "SELECT",
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leCityObj) => new CityEntity()
                    {
                        CityId=leCityObj?.CityId,
                        CityName = leCityObj?.CityName
                    })
                    ?.ToList();

                return getCityData;
            });
        }

        private async Task<string> InsertHobbiesData(decimal? userId, List<HobbyEntity> hobbyEntityObj)
        {
            int? status = null;
            string message = null;


            return await Task.Run(() =>
            {
                foreach (HobbyEntity val in hobbyEntityObj)
                {
                    var setQuery = dc?.uspSetHobbies(
                      "INSERT",
                      val?.HobbyId,
                      val?.HobbyName,
                      val?.Interested,
                      userId,
                      ref status,
                      ref message
                      );
                }


                return message;

            });
        }

        protected  async override Task<string> InsertUserData(UserEntity userEntityObj)
        {
            decimal? userId = null;
            int? status = null;
            string message = null;


            return await Task.Run(async () =>
            {
                var setQuery = dc?.uspSetUser(
                    "INSERT",
                    userEntityObj?.UserId,
                    userEntityObj?.FirstName,
                    userEntityObj?.LastName,
                    Convert.ToDateTime(userEntityObj?.DateOfBirth),
                    userEntityObj?.Age,
                    userEntityObj?.Gender,
                    Convert.ToInt32(userEntityObj?.CityId),
                    userEntityObj?.ImagePath,
                    userEntityObj?.MobileNo,
                    ref userId,
                    ref status,
                    ref message
                    );

                if (userId != null)
                {
                    await this.InsertHobbiesData(userId, userEntityObj?.hobbyEntityObj);
                }

                return message;
            });
        }

        #endregion
    }
}
