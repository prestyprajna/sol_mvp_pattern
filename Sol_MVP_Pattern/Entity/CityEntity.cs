﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class CityEntity
    {
        public decimal? CityId { get; set; }

        public string CityName { get; set; }
    }
}
