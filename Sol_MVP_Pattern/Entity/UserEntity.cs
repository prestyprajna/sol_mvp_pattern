﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool? Gender { get; set; }

        public string DateOfBirth { get; set; }

        public int? Age { get; set; }

        public int? CityId { get; set; }

        public string ImagePath { get; set; }

        public string MobileNo { get; set; }

        public List<HobbyEntity> hobbyEntityObj { get; set; }
    }
}
