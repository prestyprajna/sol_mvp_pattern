﻿using DAL;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class UserContext: UserDal
    {
        public override Task<List<CityEntity>> GetCityData()
        {
            return base.GetCityData();
        }

        protected override Task<string> InsertUserData(UserEntity userEntityObj)
        {
            return base.InsertUserData(userEntityObj);
        }
    }
}
